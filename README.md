# Automação web com Cypress

Projeto criado para o desafio técnico para QA no Sicredi.

André Luiz Muniz dos Reis

andretidf@gmail.com

https://www.linkedin.com/in/andreluizmuniz/


## Cypress 12.1.0
Site utilizado nos testes - https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap

pasta cypress/e2e:
- Preencher e salvar o formulário com sucesso
- Procurar o item cadastrado e deletar

Foi criado um custom command que preenche e envia o formulário/verifica o registro e deleta(support/commands)



## Integração - GitLab CI

Rodando e salvando artefatos que podem ser baixados na aba CI/CD - videos e screenshots

## Obrigado


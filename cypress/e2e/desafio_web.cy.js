
describe('Desafio Sicredi Web - Cadasto e Exclusão - Cypress', () => {
  
  beforeEach(() => {
    cy.visit('https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap')
    cy.get('#switch-version-select').select('Bootstrap V4 Theme')
    cy.get('.floatL.t5 > .btn').click()
})

  it('Desaio 1 - Preencher e salvar o formulário com sucesso', () => {
    cy.fillAllFieldsAndSubmit()

    cy.get('#report-success').should('contain.text', 'Your data has been successfully stored into the database. Edit Record or Go back to list')


  })

  it('Desaio 2 - Procurar o item cadastrado e deletar com sucesso', () => {
    cy.fillAllFieldsAndSubmit()
    cy.get('#save-and-go-back-button').click()
    cy.checkRegistryAndDelete()

    cy.get('.alert-delete-multiple-one').should('contain.text', 'Are you sure that you want to delete this 1 item?')
    cy.get('.delete-multiple-confirmation .btn-danger').click()
    cy.contains('[data-growl="message"]','Your data has been successfully deleted from the database').should('to.be.visible')
})

afterEach(() => {
  cy.screenshot()
})
})